# Creates the Lambda function and adds the execution code from the file the S3_key location. The zip file name 
# and the handler name must match.
resource "aws_lambda_function" "unicorn_request" {
  s3_bucket     = "${var.s3_bucket_name}"
  s3_key        = "js/lambda_code/requestUnicorn.zip"
  function_name = "${var.lambda_function_name}"
  role          = "${aws_iam_role.iam_for_lambda.arn}"
  handler       = "requestUnicorn.handler"
  description   = "Core function to process API requests from the web application to dispatch a unicorn"
  runtime = "nodejs10.x"
  depends_on = ["null_resource.upload_to_s3"]
}