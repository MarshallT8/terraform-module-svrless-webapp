# The S3 Bucket configured for static website hosting
resource "aws_s3_bucket" "hostbucket" {
  bucket = "${var.s3_bucket_name}"
  acl    = "public-read"
  force_destroy = true
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement":  [
        {
            "Effect": "Allow", 
            "Principal": "*", 
            "Action": "s3:GetObject", 
            "Resource": "arn:aws:s3:::${var.s3_bucket_name}/*"
        } 
    ]
}
POLICY
 website {
    index_document = "index.html"
    
  }
}
resource "null_resource" "upload_to_s3" {
  provisioner "local-exec" {
    command = "aws s3 sync /vagrant/sites/wildrides s3://${var.s3_bucket_name} --region eu-west-2"
  }
  depends_on = ["aws_s3_bucket.hostbucket"]
}