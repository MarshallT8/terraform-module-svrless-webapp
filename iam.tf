# Creates a IAM role that Lambda will use to access other resources
resource "aws_iam_role" "iam_for_lambda" {
  name = "${var.aws_iam_role_name}" # select project/lambda based name e.g. WildRydesLambda

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
# Creates a policy that allows write access to the database table
resource "aws_iam_policy" "dynamodb_pol" {
  name = "DynamoDBWriteAccess"
  
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": "dynamodb:PutItem",
          "Resource": "${aws_dynamodb_table.rides.arn}"
      }
  ]
}
EOF
}
# Attaches a managed policy for basic execution to the Lambda IAM role
resource "aws_iam_role_policy_attachment" "attach_managed" {
  role       = "${aws_iam_role.iam_for_lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
# Attaches the custom policy created above for writing to the database table
resource "aws_iam_policy_attachment" "attach_custom" {
  roles       = ["${aws_iam_role.iam_for_lambda.name}"]
  name       = "custom_attach"
  policy_arn = "${aws_iam_policy.dynamodb_pol.arn}"
  
}