# Adds the Cognito User Pool - User pools are user directories that provide sign-up and sign-in options for your
# app users
resource "aws_cognito_user_pool" "pool" {
  name = "${var.cognito_userpool_name}"
  auto_verified_attributes = [
          "email"
        ]
 email_configuration {
            email_sending_account = "COGNITO_DEFAULT"
        }

        password_policy {
            minimum_length    = 8
            require_lowercase = true
            require_numbers   = true
            require_symbols   = true
            require_uppercase = true
        }

        verification_message_template {
            default_email_option = "CONFIRM_WITH_CODE"
        }
}
# Adds the Cognito User Pool Client - An app is an entity within a user pool that has permission to call 
# unauthenticated APIs (APIs that do not have an authenticated user), such as APIs to register, sign in, 
# and handle forgotten passwords. To call these APIs, you need an app client ID and an optional client secret.
# It is your responsibility to secure any app client IDs or secrets so that only authorized client apps can call 
# these unauthenticated APIs.
resource "aws_cognito_user_pool_client" "client" {
  name = "${var.cognito_client_name}"

    user_pool_id = "${aws_cognito_user_pool.pool.id}"
}

